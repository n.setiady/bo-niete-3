﻿using System;
using System.Windows;

namespace WpfApp1
{
    class Catiaübergabe
    {
        Catiaverbindung CV = new Catiaverbindung();
        public Catiaübergabe()
        {
            if (CV.CATIALaeuft())
            {
                // Part erstellen
                CV.ErzeugePart();

                CV.ErstelleLeereSkizze();

                CV.ErstelleKreis();
            }
            else
            {
                MessageBox.Show("Bitte starten Sie Catia.");
            }
        }
    }
}