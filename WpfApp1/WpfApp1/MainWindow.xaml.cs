﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.CompilerServices;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {       
        public MainWindow()
        {
            InitializeComponent();
        }

        

        public void Fehler_bedingung()
        {
            if (double.Parse(txt_d1.Text) <= 0 || double.Parse(txt_länge_d1.Text) <= 0)
            {
                MessageBox.Show("Tippen Sie bitte einen Wert mehr als 0 ein. Der Fehler liegt in der ersten Zeile.");
            }
            else if (double.Parse(txt_d2.Text) <= 0 || double.Parse(txt_länge_d2.Text) <= 0)
            {
                MessageBox.Show("Tippen Sie bitte einen Wert mehr als 0 ein. Der Fehler liegt in der zweiten Zeile.");
            }
            else if (double.Parse(txt_d3.Text) <= 0 || double.Parse(txt_länge_d3.Text) <= 0)
            {
                MessageBox.Show("Tippen Sie bitte einen Wert mehr als 0 ein. Der Fehler liegt in der dritten Zeile.");
            }
            else if (double.Parse(txt_d4.Text) <= 0 || double.Parse(txt_länge_d4.Text) <= 0)
            {
                MessageBox.Show("Tippen Sie bitte einen Wert mehr als 0 ein. Der Fehler liegt in der vierten Zeile.");
            }
            else if (double.Parse(txt_d5.Text) <= 0 || double.Parse(txt_länge_d5.Text) <= 0)
            {
                MessageBox.Show("Tippen Sie bitte einen Wert mehr als 0 ein. Der Fehler liegt in der fünften Zeile.");
            }
            else if (material1.SelectedIndex == -1)
            {
                MessageBox.Show("Wählen Sie bitte ein Material aus");
            }
            else if (rb_einheit_index1.IsChecked == false && rb_einheit_index2.IsChecked == false)
            {
                MessageBox.Show("Wählen Sie bitte eine Volumeneinheit aus");
            }
            else if (rb_euro.IsChecked == false && rb_dollar.IsChecked == false)
            {
                MessageBox.Show("Wählen Sie bitte eine Devise aus");
            }
        }

        public double Feature()
        {
            double a = 2.0; // Preis für index 0
            double b = 3.0; // für index 1
            double c = 4.0; //..
            if (cb_Features_1.SelectedIndex == 0)
            {
                if (cb_Features_2.SelectedIndex == 0)
                {

                    return 2 * a;
                }
                else if (cb_Features_2.SelectedIndex == 1)
                {
                    return a + b;
                }
                else if (cb_Features_2.SelectedIndex == 2)
                {
                    return a + c;
                }
                else return a;
            }
            else if (cb_Features_1.SelectedIndex == 1)
            {
                if (cb_Features_2.SelectedIndex == 0)
                {

                    return a + b;
                }
                else if (cb_Features_2.SelectedIndex == 1)
                {
                    return 2 * b;
                }
                else if (cb_Features_2.SelectedIndex == 2)
                {
                    return b + c;
                }
                else return b;
            }
            else if (cb_Features_1.SelectedIndex == 2)
            {
                if (cb_Features_2.SelectedIndex == 0)
                {

                    return c + a;
                }
                else if (cb_Features_2.SelectedIndex == 1)
                {
                    return c + b;
                }
                else if (cb_Features_2.SelectedIndex == 2)
                {
                    return 2 * c;
                }
                else return c;
            }


            else return 0.0;

        }

        public void bt_berechnen_Click(object sender, RoutedEventArgs e)
        {
            d_Einzel d1 = new();
            try
            {
                d1.Durchmesser = double.Parse(txt_d1.Text);
                d1.Länge = double.Parse(txt_länge_d1.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe von Wahl entspricht nicht dem benötigten Format.");
            }

            d_Einzel d2 = new();
            try
            {
                d2.Durchmesser = double.Parse(txt_d2.Text);
                d2.Länge = double.Parse(txt_länge_d2.Text);
            }

            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe von Wahl entspricht nicht dem benötigten Format.");
            }

            d_Einzel d3 = new();
            try
            {
                d3.Durchmesser = double.Parse(txt_d3.Text);
                d3.Länge = double.Parse(txt_länge_d3.Text);
            }

            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe von Wahl entspricht nicht dem benötigten Format.");
            }

            d_Einzel d4 = new();
            try
            {
                d4.Durchmesser = double.Parse(txt_d4.Text);
                d4.Länge = double.Parse(txt_länge_d4.Text);


            }

            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe von Wahl entspricht nicht dem benötigten Format.");
            }

            d_Einzel d5 = new();
            try
            {
                d5.Durchmesser = double.Parse(txt_d5.Text);
                d5.Länge = double.Parse(txt_länge_d5.Text);

                // Fehler_bedingung();
            }

            catch (FormatException)
            {
                MessageBox.Show("Die Eingabe von Wahl entspricht nicht dem benötigten Format.");
            }


            string einheit;

            if (rb_einheit_index1.IsChecked == true)
            {

                einheit = rb_einheit_index1.Content.ToString();
                txt_volumen.Text = Convert.ToString(Volumen_berechnung() + " mm³");
                txt_spanvolumen.Text = Convert.ToString(Spanvolumen_berechnung() + " mm³");
            }                                                                                                    // Umrechnung der volumen und zeigen
            else if (rb_einheit_index2.IsChecked == true)
            {
                einheit = rb_einheit_index2.Content.ToString();
                txt_volumen.Text = Convert.ToString(Math.Round(Volumen_berechnung() / 1000, 5) + " cm³");
                txt_spanvolumen.Text = Convert.ToString(Spanvolumen_berechnung() / 1000 + " cm³");
            }

            double dichte;
            double materialpreis;
            double masse;
            double rohgewicht;
            double spanpreis;

            if (rb_euro.IsChecked == true)
            {

                if (material1.SelectedIndex == 0)
                {
                    materialpreis = 1.2;
                    dichte = 0.00785;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Euro");
                }
                else if (material1.SelectedIndex == 1)
                {
                    materialpreis = 0.95;
                    dichte = 0.00725;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Euro");
                }
                else if (material1.SelectedIndex == 2)
                {
                    materialpreis = 1.015;
                    dichte = 0.0083;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Euro");
                }

            }
            if (rb_dollar.IsChecked == true)
            {
                if (material1.SelectedIndex == 0)
                {
                    materialpreis = 1.2 * 1.055;
                    dichte = 0.00785;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Dollar");
                }
                else if (material1.SelectedIndex == 1)
                {
                    materialpreis = 0.95 * 1.055;
                    dichte = 0.00725;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Dollar");
                }
                else if (material1.SelectedIndex == 2)
                {
                    materialpreis = 1.015 * 1.055;
                    dichte = 0.0083;

                    rohgewicht = Math.Round(Rohteilvol_berechnung() * dichte / 1000, 2);
                    masse = Math.Round(Volumen_berechnung() * dichte / 1000, 2);
                    spanpreis = Math.Round(Spanvolumen_berechnung() * 0.001);

                    d1.preis = Math.Round(rohgewicht * materialpreis + spanpreis + Feature(), 2);
                    txt_masse.Text = Convert.ToString(masse + " kg");
                    txt_preis.Text = Convert.ToString(d1.preis + " Dollar");
                }

            }












            double Volumen;
            double V1, V2, V3, V4, V5;
            double Spanvolumen;
            double Rohteil_d;
            double Länge_gesamt;

            double Volumen_berechnung()
            {
                V1 = d1.Volumen_berechnung();
                V2 = d2.Volumen_berechnung();
                V3 = d3.Volumen_berechnung();
                V4 = d4.Volumen_berechnung();
                V5 = d5.Volumen_berechnung();


                Volumen = Math.Round(V1 + V2 + V3 + V4 + V5);
                return Volumen;
            }
            double Spanvolumen_berechnung()
            {
                Spanvolumen = Math.Round((Rohteilvol_berechnung() - Volumen), 3);
                if (Spanvolumen < 0)
                {
                    return 0;
                }
                else
                {
                    return Spanvolumen;
                }
            }
            double Rohteilvol_berechnung()
            {
                Rohteil_d = double.Parse(txt_rohteildurchmesser.Text);
                Länge_gesamt = d1.Länge + d2.Länge + d3.Länge + d4.Länge + d5.Länge;

                return ((Rohteil_d / 2) * (Rohteil_d / 2) * Math.PI * Länge_gesamt);
            }



        }



        private void cb_Durchmesseranzahl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cb_Durchmesseranzahl.SelectedIndex == 0)        //auswahl 1
            {
                lab_d1.Visibility = Visibility.Visible;
                txt_d1.Visibility = Visibility.Visible;
                txt_d1.Text = "";
                lab_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Text = "";
                cb_Features_1.Visibility = Visibility.Visible;

                lab_d2.Visibility = Visibility.Hidden;
                txt_d2.Visibility = Visibility.Hidden;
                txt_d2.Text = "0";
                lab_länge_d2.Visibility = Visibility.Hidden;
                txt_länge_d2.Visibility = Visibility.Hidden;
                txt_länge_d2.Text = "0";
                cb_Features_2.Visibility = Visibility.Hidden;

                lab_d3.Visibility = Visibility.Hidden;
                txt_d3.Visibility = Visibility.Hidden;
                txt_d3.Text = "0";
                lab_länge_d3.Visibility = Visibility.Hidden;
                txt_länge_d3.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_3.Visibility = Visibility.Hidden;

                lab_d4.Visibility = Visibility.Hidden;
                txt_d4.Visibility = Visibility.Hidden;
                txt_d4.Text = "0";
                lab_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Text = "0";
                cb_Features_4.Visibility = Visibility.Hidden;

                lab_d5.Visibility = Visibility.Hidden;
                txt_d5.Visibility = Visibility.Hidden;
                txt_d5.Text = "0";
                lab_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_5.Visibility = Visibility.Hidden;

            }
            else if (cb_Durchmesseranzahl.SelectedIndex == 1)       //auswahl 2
            {
                lab_d1.Visibility = Visibility.Visible;
                txt_d1.Visibility = Visibility.Visible;
                txt_d1.Text = "";
                lab_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Text = "";
                cb_Features_1.Visibility = Visibility.Visible;

                lab_d2.Visibility = Visibility.Visible;
                txt_d2.Visibility = Visibility.Visible;
                txt_d2.Text = "";
                lab_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Text = "";
                cb_Features_2.Visibility = Visibility.Visible;

                lab_d3.Visibility = Visibility.Hidden;
                txt_d3.Visibility = Visibility.Hidden;
                txt_d3.Text = "0";
                lab_länge_d3.Visibility = Visibility.Hidden;
                txt_länge_d3.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_3.Visibility = Visibility.Hidden;

                lab_d4.Visibility = Visibility.Hidden;
                txt_d4.Visibility = Visibility.Hidden;
                txt_d4.Text = "0";
                lab_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Text = "0";
                cb_Features_4.Visibility = Visibility.Hidden;

                lab_d5.Visibility = Visibility.Hidden;
                txt_d5.Visibility = Visibility.Hidden;
                txt_d5.Text = "0";
                lab_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_5.Visibility = Visibility.Hidden;
            }
            else if (cb_Durchmesseranzahl.SelectedIndex == 2)       //auswahl 3 .
            {
                lab_d1.Visibility = Visibility.Visible;
                txt_d1.Visibility = Visibility.Visible;
                txt_d1.Text = "";
                lab_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Text = "";
                cb_Features_1.Visibility = Visibility.Visible;

                lab_d2.Visibility = Visibility.Visible;
                txt_d2.Visibility = Visibility.Visible;
                txt_d2.Text = "";
                lab_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Text = "";
                cb_Features_2.Visibility = Visibility.Visible;

                lab_d3.Visibility = Visibility.Visible;
                txt_d3.Visibility = Visibility.Visible;
                txt_d3.Text = "";
                lab_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Text = "";
                cb_Features_3.Visibility = Visibility.Visible;

                lab_d4.Visibility = Visibility.Hidden;
                txt_d4.Visibility = Visibility.Hidden;
                txt_d4.Text = "0";
                lab_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Visibility = Visibility.Hidden;
                txt_länge_d4.Text = "0";
                cb_Features_4.Visibility = Visibility.Hidden;

                lab_d5.Visibility = Visibility.Hidden;
                txt_d5.Visibility = Visibility.Hidden;
                txt_d5.Text = "0";
                lab_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_5.Visibility = Visibility.Hidden;
            }
            else if (cb_Durchmesseranzahl.SelectedIndex == 3)       //auswahl 4
            {
                lab_d1.Visibility = Visibility.Visible;
                txt_d1.Visibility = Visibility.Visible;
                txt_d1.Text = "";
                lab_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Text = "";
                cb_Features_1.Visibility = Visibility.Visible;

                lab_d2.Visibility = Visibility.Visible;
                txt_d2.Visibility = Visibility.Visible;
                txt_d2.Text = "";
                lab_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Text = "";
                cb_Features_2.Visibility = Visibility.Visible;

                lab_d3.Visibility = Visibility.Visible;
                txt_d3.Visibility = Visibility.Visible;
                txt_d3.Text = "";
                lab_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Text = "";
                cb_Features_3.Visibility = Visibility.Visible;

                lab_d4.Visibility = Visibility.Visible;
                txt_d4.Visibility = Visibility.Visible;
                txt_d4.Text = "";
                lab_länge_d4.Visibility = Visibility.Visible;
                txt_länge_d4.Visibility = Visibility.Visible;
                txt_länge_d4.Text = "";
                cb_Features_4.Visibility = Visibility.Visible;

                lab_d5.Visibility = Visibility.Hidden;
                txt_d5.Visibility = Visibility.Hidden;
                txt_d5.Text = "0";
                lab_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d5.Visibility = Visibility.Hidden;
                txt_länge_d3.Text = "0";
                cb_Features_5.Visibility = Visibility.Hidden;
            }
            else if (cb_Durchmesseranzahl.SelectedIndex == 4)       //auwahl 5
            {
                lab_d1.Visibility = Visibility.Visible;
                txt_d1.Visibility = Visibility.Visible;
                txt_d1.Text = "";
                lab_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Visibility = Visibility.Visible;
                txt_länge_d1.Text = "";
                cb_Features_1.Visibility = Visibility.Visible;

                lab_d2.Visibility = Visibility.Visible;
                txt_d2.Visibility = Visibility.Visible;
                txt_d2.Text = "";
                lab_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Visibility = Visibility.Visible;
                txt_länge_d2.Text = "";
                cb_Features_2.Visibility = Visibility.Visible;

                lab_d3.Visibility = Visibility.Visible;
                txt_d3.Visibility = Visibility.Visible;
                txt_d3.Text = "";
                lab_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Visibility = Visibility.Visible;
                txt_länge_d3.Text = "";
                cb_Features_3.Visibility = Visibility.Visible;

                lab_d4.Visibility = Visibility.Visible;
                txt_d4.Visibility = Visibility.Visible;
                txt_d4.Text = "";
                lab_länge_d4.Visibility = Visibility.Visible;
                txt_länge_d4.Visibility = Visibility.Visible;
                txt_länge_d4.Text = "";
                cb_Features_4.Visibility = Visibility.Visible;

                lab_d5.Visibility = Visibility.Visible;
                txt_d5.Visibility = Visibility.Visible;
                txt_d5.Text = "";
                lab_länge_d5.Visibility = Visibility.Visible;
                txt_länge_d5.Visibility = Visibility.Visible;
                txt_länge_d3.Text = "";
                cb_Features_5.Visibility = Visibility.Visible;
            }


        }

        private void txt_rohteildurchmesser_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void cb_materialen(object sender, SelectionChangedEventArgs e)
        {

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void txt_länge_d2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        public void btn_Übergabebutton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Übergabe der Werte an Catia. Dies kann einen moment dauern.");

            Catiaübergabe CV = new Catiaübergabe();
        }
     

    }

}
