﻿using System;
using System.Windows;
using HybridShapeTypeLib;
using INFITF;
using MECMOD;
using PARTITF;
//:WpfApp1.MainWindow
public class Catiaverbindung: WpfApp1.MainWindow
{
    
    INFITF.Application hsp_catiaApp;
    MECMOD.PartDocument hsp_catiaPartDoc;
    MECMOD.Sketch hsp_catiaSkizze;

    ShapeFactory SF;
    HybridShapeFactory HSF;

    Part myPart;
    Sketches mySketches;

    public double D1, D2, D3, D4, D5;
    public double L1, L2, L3, L4, L5;



    public bool CATIALaeuft()
    {
        try
        {
            
            // object catiaObject = System.Runtime.InteropServices.Marshal.GetActiveObject("CATIA.Application");
            object catiaObject = Marshal2.GetActiveObject("CATIA.Application");
            hsp_catiaApp = (INFITF.Application)catiaObject;

            return true;
        }
        catch (Exception)
        {
            MessageBox.Show(test_box.Text);
            return false;
        }
    }
    public void ErzeugePart()
    {
        INFITF.Documents catDocuments1 = hsp_catiaApp.Documents;
        hsp_catiaPartDoc = catDocuments1.Add("Part") as MECMOD.PartDocument;
        myPart = hsp_catiaPartDoc.Part;
    }
    public void ErstelleLeereSkizze()
    {
        // Factories für das Erzeugen von Modellelementen (Std und Hybrid)
        SF = (ShapeFactory)myPart.ShapeFactory;
        HSF = (HybridShapeFactory)myPart.HybridShapeFactory;

        // geometrisches Set auswaehlen und umbenennen
        HybridBodies catHybridBodies1 = myPart.HybridBodies;
        HybridBody catHybridBody1;

        try
        {
            catHybridBody1 = catHybridBodies1.Item("Geometrisches Set.1");
        }
        catch (Exception)
        {
            MessageBox.Show("Kein geometrisches Set gefunden! " + Environment.NewLine +
                "Ein PART manuell erzeugen und ein darauf achten, dass 'Geometisches Set' aktiviert ist.");
           
            return;
        }
        catHybridBody1.set_Name("Welle");

        // neue Skizze im ausgewaehlten geometrischen Set auf eine Offset Ebene legen
        mySketches = catHybridBody1.HybridSketches;
        OriginElements catOriginElements = myPart.OriginElements;
        HybridShapePlaneOffset hybridShapePlaneOffset1 = HSF.AddNewPlaneOffset(
            (Reference)catOriginElements.PlaneYZ, 0, false);
        hybridShapePlaneOffset1.set_Name("OffsetEbene");
        catHybridBody1.AppendHybridShape(hybridShapePlaneOffset1);
        myPart.InWorkObject = hybridShapePlaneOffset1;
        myPart.Update();

        HybridShapes hybridShapes1 = catHybridBody1.HybridShapes;
        Reference catReference1 = (Reference)hybridShapes1.Item("OffsetEbene");

        hsp_catiaSkizze = mySketches.Add(catReference1);

        // Achsensystem in Skizze erstellen 
        ErzeugeAchsensystem();

        // Part aktualisieren
        myPart.Update();
    }
    private void ErzeugeAchsensystem()
    {
        object[] arr = new object[] {0.0, 0.0, 0.0,
                                     0.0, 1.0, 0.0,
                                     0.0, 0.0, 1.0 };
        hsp_catiaSkizze.SetAbsoluteAxisData(arr);
    }

    public void ErstelleKreis()
    {
        hsp_catiaSkizze.set_Name("Rotationsteile");

        Factory2D catFactory2D1 = hsp_catiaSkizze.OpenEdition();
        try
        {
            D1 = double.Parse(txt_d1.Text);
            D2 = double.Parse(txt_d2.Text);
            D3 = double.Parse(txt_d3.Text);
            D4 = double.Parse(txt_d4.Text);
            D5 = double.Parse(txt_d5.Text);
            

            L1 = double.Parse(txt_länge_d1.Text);
            L2 = double.Parse(txt_länge_d2.Text);
            L3 = double.Parse(txt_länge_d3.Text);
            L4 = double.Parse(txt_länge_d4.Text);
            L5 = double.Parse(txt_länge_d5.Text);


        }
        catch
        {
            MessageBox.Show("Etwas ist Schiefgegangen bei der Übergabe der Werte!");
        }

        if (cb_Durchmesseranzahl.SelectedIndex == 0)
        {
            Point2D catPoint2D1 = catFactory2D1.CreatePoint(0, 0);
            Point2D catPoint2D2 = catFactory2D1.CreatePoint(0, D1);
            Point2D catPoint2D3 = catFactory2D1.CreatePoint(L1, D1);
            Point2D catPoint2D4 = catFactory2D1.CreatePoint(L1, 0);

            Line2D catLine2D1 = catFactory2D1.CreateLine(0, 0, 0, D1);
            catLine2D1.StartPoint = catPoint2D1;
            catLine2D1.EndPoint = catPoint2D2;

            Line2D catLine2D2 = catFactory2D1.CreateLine(0, D1, L1, D1);
            catLine2D2.StartPoint = catPoint2D2;
            catLine2D2.EndPoint = catPoint2D3;

            Line2D catLine2D3 = catFactory2D1.CreateLine(L1, D1, L1, 0);
            catLine2D3.StartPoint = catPoint2D3;
            catLine2D3.EndPoint = catPoint2D4;

            Line2D catLine2D4 = catFactory2D1.CreateLine(L1, 0, 0, 0);
            catLine2D4.StartPoint = catPoint2D4;
            catLine2D4.EndPoint = catPoint2D1;
        }
        else if (cb_Durchmesseranzahl.SelectedIndex == 1)
        {
            Point2D catPoint2D1 = catFactory2D1.CreatePoint(0, 0);
            Point2D catPoint2D2 = catFactory2D1.CreatePoint(0, D1);
            Point2D catPoint2D3 = catFactory2D1.CreatePoint(L1, D1);
            Point2D catPoint2D4 = catFactory2D1.CreatePoint(L1, D2);
            Point2D catPoint2D5 = catFactory2D1.CreatePoint((L1 + L2), D2);
            Point2D catPoint2D6 = catFactory2D1.CreatePoint((L1 + L2), 0);

            Line2D catLine2D1 = catFactory2D1.CreateLine(0, 0, 0, D1);
            catLine2D1.StartPoint = catPoint2D1;
            catLine2D1.EndPoint = catPoint2D2;

            Line2D catLine2D2 = catFactory2D1.CreateLine(0, D1, L1, D1);
            catLine2D2.StartPoint = catPoint2D2;
            catLine2D2.EndPoint = catPoint2D3;

            Line2D catLine2D3 = catFactory2D1.CreateLine(L1, D1, L1, D2);
            catLine2D3.StartPoint = catPoint2D3;
            catLine2D3.EndPoint = catPoint2D4;

            Line2D catLine2D4 = catFactory2D1.CreateLine((L1+L2), D2, (L1+L2), D2);
            catLine2D4.StartPoint = catPoint2D4;
            catLine2D4.EndPoint = catPoint2D5;

            Line2D catLine2D5 = catFactory2D1.CreateLine((L1 + L2), D2, (L1 + L2),0) ;
            catLine2D5.StartPoint = catPoint2D5;
            catLine2D5.EndPoint = catPoint2D6;

            Line2D catLineD6 = catFactory2D1.CreateLine((L1+L2),0,0,0);
            catLine2D5.StartPoint = catPoint2D6;
            catLine2D5.EndPoint = catPoint2D1;
        }
        hsp_catiaSkizze.CloseEdition();

        myPart.Update();

        Rotieren();
    }
    public void Rotieren()
    {




        myPart.Update();
    }

}