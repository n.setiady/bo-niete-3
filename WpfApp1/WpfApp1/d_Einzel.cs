﻿    using System;

    public class d_Einzel
    {
        public double Länge { set; get; }
        public double Durchmesser { set; get; }
        public double Rohteil_d { set; get; }
        public double preis { set; get; }
        public double Volumen_berechnung()
        {

            return Math.Round(Länge * (Durchmesser / 2) * (Durchmesser / 2) * Math.PI, 3);              // Berechnung des Volumens mit 3 Zähle nach dem Komma

        }


        public double Rohteilvol_berechnung()
        {

            return Math.Round((Rohteil_d / 2) * (Rohteil_d / 2) * Länge * Math.PI, 3);

        }

    //kommentar
    }
